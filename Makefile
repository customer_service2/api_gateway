pull_submodule:
	git submodule update --init --recursive

update_submodule:	
	git submodule update --remote --merge
	
run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:customer_service2/protos.git

swag:
	swag init -g ./api/router.go -o api/docs

create_migrate:
	migrate create -ext sql -dir migrations -seq create_revie_table

# create table in database
migrate_up:
	migrate -source file://migrations -database postgres://postgres:3301@localhost:5432/customerdb?sslmode=disable up

migrate_down:
	migrate -source file://migrations -database postgres://postgres:3301@localhost:5432/customerdb?sslmode=disable down

migrate_fix:
	migrate -path migrations/ -database postgres://postgres:3301@localhost:5432/customerdb?sslmode=disable force 1