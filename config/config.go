package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment string // develop, staging, production

	CustomerServiceHost string
	CustomerServicePort int

	PostServiceHost string
	PostServicePort int

	ReviewServiceHost string
	ReviewServicePort int

	RedisHost string
	RedisPort string

	CtxTimeout int

	LogLevel  string
	HTTPPort  string
	SignInKey string
}

func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOGLEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":9090"))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToInt(getOrReturnDefault("USER_SERVICE_PORT", 3000))

	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "localhost"))
	c.PostServicePort = cast.ToInt(getOrReturnDefault("POST_SERVICE_PORT", 7000))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToInt(getOrReturnDefault("REVIEW_SERVICE_PORT", 5000))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST","localhost"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT","6379"))

	c.SignInKey = cast.ToString(getOrReturnDefault("SINGINGKEY","develop_2002"))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIME_OUT", 7))
	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
