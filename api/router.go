package api

import (
	_ "api_gateway/api/docs"
	v1 "api_gateway/api/handlers/v1"
	"api_gateway/config"
	"api_gateway/pkg/logger"
	"api_gateway/services"
	"api_gateway/storage/repo"

	"github.com/gin-gonic/gin"
	swaggerfile "github.com/swaggo/files"

	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.RedisRepo
}

func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
	})

	api := router.Group("/v1")
	// customer service post, get...
	api.POST("/customer", handlerV1.Create)
	api.GET("/customer/post/review/:id", handlerV1.GetCustomerInfo)
	api.PATCH("/customer/update", handlerV1.UpdateCustomer)
	api.DELETE("/customer/:id", handlerV1.DeleteCustomer)

	//post service post, get...
	api.POST("/posts", handlerV1.CreatePost)
	api.GET("/posts/review/get/:id", handlerV1.GetPostReview)
	api.PATCH("/posts/update/", handlerV1.UpdatePost)
	api.DELETE("/posts/review/delete/:id", handlerV1.DeletePost)

	//review service post, get...
	api.POST("/review", handlerV1.CreateReview)
	api.GET("/review/:id", handlerV1.GetReviewById)
	api.PATCH("/review/update", handlerV1.UpdateReview)
	api.DELETE("/review/delete/:id", handlerV1.DeleteReview)
	api.GET("/reviews", handlerV1.Pagelist)

	// register customer
	api.POST("/customer/register", handlerV1.RegisterCustomer)
	api.GET("/verify/:email/:code", handlerV1.Verify)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerfile.Handler, url))

	return router
}
