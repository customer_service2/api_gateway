package v1

import (
	"api_gateway/api/models"
	"api_gateway/email"
	"api_gateway/genproto/customer"
	"api_gateway/pkg/etc"
	"api_gateway/pkg/logger"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// register customer
// @Summary		register customer
// @Description	this registers customer
// @Tags		Customer
// @Accept		json
// @Produce 	json
// @Param 		body	body  models.CustomerRegister true "Register customer"
// @Success		201 	{object} models.Error
// @Failure		500 	{object} models.Error
// @Router		/v1/customer/register 	[post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var body customer.CustomerRequest

	err := c.ShouldBindJSON(&body)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("couldn't hash the password")
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	emailExists, err := h.serviceManager.CustomerService().CheckField(ctx, &customer.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("Error while cheking email uniqeness", logger.Any("check", err))
		return
	}

	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Description:"this email exists",
		})
		return
	}
	exists, err := h.redis.Exists(body.Email)
	fmt.Println(body.Email)
	fmt.Println(exists)
	if err != nil {
		h.log.Error("Error while checking to redis email")
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}
	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Description:"this email exists",		
		})
		return
	}

	if cast.ToInt(exists) == 1 {
		c.JSON(http.StatusConflict, models.Error{
			Description:"this email exists",		
		})
		return
	}
	customerToSaved := &customer.CustomerRequest{
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		Email:       body.Email,
		PhoneNumber: body.PhoneNumber,
		Password:    body.Password,
	}
	for _, address := range body.Addresses {
		customerToSaved.Addresses = append(customerToSaved.Addresses, &customer.AddressReq{
			Country: address.Country,
			Street:  address.Street,
		})
	}
	customerToSaved.Code = etc.GenerateCode(6)
	msg := "Subject: Customer email verification\n Your verification code: " + customerToSaved.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}
	c.JSON(http.StatusAccepted, models.Error{
		Error:       nil,
		Code:        http.StatusAccepted,
		Description: "Your request successfuly accepted we have send code to your email, Your code is : " + customerToSaved.Code,
	}) // Code should be sent to gmail

	bodyByte, err := json.Marshal(customerToSaved)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
	err = h.redis.SetWithTTL(customerToSaved.Email, string(bodyByte), 300)
	if err != nil {
		h.log.Error("Error while marshaling to json", logger.Any("json", err))
		return
	}
}

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Customer
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.VerifyResponse
// @Router      /verify/{email}/{code} [get]
func (h *handlerV1) Verify(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)

	customerbody, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting customer from redis", logger.Any("redis", err))
	}
	customerBodys := cast.ToString(customerbody)
	body := customer.CustomerRequest{}
	err = json.Unmarshal([]byte(customerBodys), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to customer body", logger.Any("json", err))
		return
	}
	fmt.Println(code)
	if body.Code != code {
		fmt.Println(body.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	fmt.Println(body.Code)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().Create(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", logger.Any("post", err))
		return
	}
	response := &models.VerifyResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Bio:         res.Bio,
		Email:       res.Email,
		PhoneNumber: res.PhoneNumber,
	}
	for _, address := range res.Addresses {
		response.Addresses = append(response.Addresses, models.AddressResponse{
			Id:      address.Id,
			OwnerId: address.CustomerId,
			Country: address.Country,
			Street:  address.Street,
		})

		h.jwthandler.Iss = res.FirstName
		h.jwthandler.Sub = res.Id
		h.jwthandler.Role = "user"
		h.jwthandler.Aud = []string{"project-app"}
		h.jwthandler.SigninKey = h.cfg.SignInKey
		tokens, err := h.jwthandler.GenerateAuthJWT()
		addressToken := tokens[0]
		refreshToken := tokens[1]

		if err != nil {
			h.log.Error("error occured while generating tokens")
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": "something went wrong,please try again",
			})
			return
		}
		response.JWT = addressToken
		response.RefreshToken = refreshToken

		c.JSON(http.StatusOK, response)
	}
}
