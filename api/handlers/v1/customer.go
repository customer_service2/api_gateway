package v1

import (
	_ "api_gateway/api/models"
	"api_gateway/genproto/customer"
	l "api_gateway/pkg/logger"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// create customer
// @Summary this function creates the customers
// @Description this function includes all customers
// @Tags Customer
// @Accept json
// @Produce json
// @Param body body models.Customer true "Customer"
// @Success 200 {object} customer.CustomerResponse
// @Failure 400 "ErrorResponse"
// @Router /v1/customer [post]
func (h *handlerV1) Create(c *gin.Context) {
	var (
		body        customer.CustomerRequest
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true
	err := c.ShouldBindJSON(&body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().Create(ctx, &body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create customer", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// get customer
// @Summary this function getting the customers posts
// @Description this function select the customers posts
// @Tags Customer
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Success 200 {object} customer.CustomerInfo
// @Failure 400 "ErrorResponse"
// @Router /v1/customer/post/review/{id} [get]
func (h *handlerV1) GetCustomerInfo(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseEnumNumbers = true

	var body customer.CustomerID

	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responseCus, err := h.serviceManager.CustomerService().GetCustomerInfo(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get customer", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, responseCus)
}

// user update
// @Summary this function update
// @Description this function updating the customers
// @Tags Customer
// @Accept json
// @Produce json
// @Param customerbody body customer.CustomerUp true "Update Customer"
// @Success 200 {object} customer.CustomerResponse
// @Failure 400 "ErrorResponse"
// @Router /v1/customer/update [patch]
func (h *handlerV1) UpdateCustomer(c *gin.Context) {
	var (
		customerbody customer.CustomerUp
		jspbMarshal  protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	err := c.ShouldBindJSON(&customerbody)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.CustomerService().UpdateCustomer(ctx, &customer.CustomerUp{
		Id:          customerbody.Id,
		FirstName:   customerbody.FirstName,
		LastName:    customerbody.LastName,
		Bio:         customerbody.Bio,
		Addresses:   customerbody.Addresses,
		Email:       customerbody.Email,
		PhoneNumber: customerbody.PhoneNumber,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}
	c.JSON(http.StatusCreated, response)
}

// delete customer
// @Summary this function delete
// @Description this function delting customer
// @Tags Customer
// @Accept json
// @Produce json
// @Param id path int true "delete Customer"
// @Success 200 {object}  customer.CustomerResponse
// @Failure 400 "ErrorResponse"
// @Router /v1/customer/{id} [delete]
func (h *handlerV1) DeleteCustomer(c *gin.Context) {

	var (
		body        customer.CustomerID
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseEnumNumbers = true

	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	customerResponse, err := h.serviceManager.CustomerService().DeleteCustomer(ctx, &body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}

	c.JSON(http.StatusCreated, customerResponse)

}
