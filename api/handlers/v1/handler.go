package v1

import (
	t "api_gateway/api/tokens"
	"api_gateway/config"
	"api_gateway/pkg/logger"
	"api_gateway/services"
	"api_gateway/storage/repo"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	cfg            config.Config
	redis          repo.RedisRepo
	jwthandler     t.JWTHandler
}

type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Cfg            config.Config
	Redis          repo.RedisRepo
	JWTHandler     t.JWTHandler
}

func New(c *HandlerV1Config) handlerV1 {
	return handlerV1{
		log:            c.Logger,
		serviceManager: c.ServiceManager,
		cfg:            c.Cfg,
		redis:          c.Redis,
		jwthandler:     c.JWTHandler,
	}
}

func GetClaims(h handlerV1, c *gin.Context) (*t.CustomClaims, error) {
	var (
		claims = t.CustomClaims{}
	)
	strToken := c.GetHeader("Authorization")
	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) { return []byte(h.cfg.SignInKey), nil })
	if err != nil {
		fmt.Println(err)
		h.log.Error("invalid access token")
		return nil, err
	}

	claims.Token = token
	return &claims, nil
}
