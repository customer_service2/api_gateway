package models

type Post struct {
	Owner_id    string
	Name        string
	Description string
	Medias      []Media
}
type Media struct {
	Name string
	Link string
	Type string
}
type CustomerRegister struct {
	FirstName   string
	LastName    string
	Bio         string
	Email       string
	Password    string
	PhoneNumber string
}
type Customer struct {
	FirstName   string
	LastName    string
	Bio         string
	Email       string
	Password    string
	PhoneNumber string
	Addresses   []Address
}
type Address struct {
	Country string
	Street  string
}

type Error struct {
	Code        int
	Error       error
	Description string
}
type VerifyResponse struct {
	Id           string
	FirstName    string
	LastName     string
	Bio          string
	Email        string
	PhoneNumber  string
	JWT          string
	RefreshToken string
	Addresses    []AddressResponse
}

type AddressResponse struct {
	Id      string
	OwnerId string
	Country string
	Street  string
}
